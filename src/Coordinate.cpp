#include "Coordinate.h"


Coordinate::Coordinate(const unsigned x, const unsigned y): m_x(x), m_y(y)
{
}

int Coordinate::Get_x() const
{
    return m_x;
}

int Coordinate::Get_y() const
{
    return m_y;
}

bool Coordinate::operator==(const Coordinate& rhs) const
{
    if (this->Get_x() == rhs.Get_x() && this->Get_y() == rhs.Get_y())
    {
        return true;
    }

    return false;
}

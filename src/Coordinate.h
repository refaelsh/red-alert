#pragma once


class Coordinate
{
public:
    Coordinate(unsigned int x, unsigned int y);

    int Get_x() const;
    int Get_y() const;

    bool operator== (const Coordinate &rhs) const;

private:
    int m_x;
    int m_y;
};

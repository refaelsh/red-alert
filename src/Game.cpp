#include "Game.h"
#include "Randomizer.h"


Game::Game(IRandomizerFactory* randomizer_factory): m_randomizer_factory(randomizer_factory)
{
    this->Clear_all_vessels();
}

void Game::Clear_all_vessels()
{
    m_vessels = std::vector<Vessel>();
}

void Game::Place_vessels()
{
    this->Clear_all_vessels();

    auto forbidden_coordinates = std::vector<Coordinate>();

    auto randomizer = m_randomizer_factory->Create(forbidden_coordinates);
    auto coordinates = randomizer->Randomize(3);
    auto vessel = Vessel(coordinates);
    m_vessels.emplace_back(vessel);

    for (auto forbidden_coordinate : vessel.Get_forbidden_coordinates_including_the_vessel())
    {
        forbidden_coordinates.emplace_back(forbidden_coordinate);
    }

    randomizer = m_randomizer_factory->Create(forbidden_coordinates);
    coordinates = randomizer->Randomize(2);
    vessel = Vessel(coordinates);
    m_vessels.emplace_back(vessel);

    for (auto forbidden_coordinate : vessel.Get_forbidden_coordinates_including_the_vessel())
    {
        forbidden_coordinates.emplace_back(forbidden_coordinate);
    }

    randomizer = m_randomizer_factory->Create(forbidden_coordinates);
    coordinates = randomizer->Randomize(1);
    vessel = Vessel(coordinates);
    m_vessels.emplace_back(vessel);
}

std::vector<Vessel> Game::Get_all_vessels() const
{
    return m_vessels;
}

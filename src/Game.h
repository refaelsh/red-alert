#pragma once
#include <vector>

#include "IRandomizerFactory.h"
#include "Vessel.h"


class Game
{
public:
    explicit Game(IRandomizerFactory* randomizer_factory);

    void Clear_all_vessels();

    void Place_vessels();

    std::vector<Vessel> Get_all_vessels() const;

private:
    IRandomizerFactory* m_randomizer_factory;
    std::vector<Vessel> m_vessels;
};

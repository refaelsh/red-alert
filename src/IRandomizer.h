#pragma once
#include <vector>
#include "coordinate.h"


class IRandomizer
{
public:
    virtual ~IRandomizer() = default;

    virtual std::vector<Coordinate> Randomize(unsigned int vessel_size) = 0;
};

#pragma once
#include <memory>
#include <vector>
#include "coordinate.h"
#include "IRandomizer.h"


class IRandomizerFactory
{
public:
    virtual ~IRandomizerFactory() = default;

    virtual std::shared_ptr<IRandomizer> Create(std::vector<Coordinate> forbidden_coordinates) = 0;
};

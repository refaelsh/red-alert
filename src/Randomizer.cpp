#include "Randomizer.h"
#include <iostream>
#include <random>
#include <utility>


Randomizer::Randomizer(std::vector<Coordinate> forbidden_coordinates): m_forbidden_coordinates(
    std::move(forbidden_coordinates))
{
}

std::vector<Coordinate> Randomizer::Randomize(const unsigned int vessel_size)
{
    auto res = std::vector<std::vector<Coordinate>>();

    if (Is_horizontal_was_randomized() == true)
    {
        for (int y = 0; y < 10; ++y)
        {
            for (unsigned int x = 0; x <= 10 - vessel_size; ++x)
            {
                auto potential_location = std::vector<Coordinate>();
                for (unsigned int i = 0; i < vessel_size; ++i)
                {
                    potential_location.emplace_back(Coordinate(x + i, y));
                }

                if (Is_at_least_one_forbidden(potential_location) == true)
                {
                    continue;
                }
                else
                {
                    res.emplace_back(potential_location);
                }
            }
        }
    }
    else
    {
        for (int x = 0; x < 10; ++x)
        {
            for (unsigned int y = 0; y <= 10 - vessel_size; ++y)
            {
                auto potential_location = std::vector<Coordinate>();
                for (unsigned int i = 0; i < vessel_size; ++i)
                {
                    potential_location.emplace_back(Coordinate(x, y + i));
                }

                if (Is_at_least_one_forbidden(potential_location) == true)
                {
                    continue;
                }
                else
                {
                    res.emplace_back(potential_location);
                }
            }
        }
    }

    const auto random_number = Pick_a_random_number(0, res.size() - 1);
    return res[random_number];
}

bool Randomizer::Is_at_least_one_forbidden(const std::vector<Coordinate>& coordinates)
{
    for (auto coordinate : coordinates)
    {
        for (auto forbidden_coordinate : m_forbidden_coordinates)
        {
            if (coordinate == forbidden_coordinate)
            {
                return true;
            }
        }
    }

    return false;
}

bool Randomizer::Is_horizontal_was_randomized()
{
    const auto res = this->Pick_a_random_number(0, 1);

    if (res == 1)
    {
        return true;
    }

    return false;
}

unsigned int Randomizer::Pick_a_random_number(const int start, const size_t end)
{
    std::random_device random_device;

    std::default_random_engine engine(random_device());
    const std::uniform_int_distribution<int> uniform_dist(start, end);
    return uniform_dist(engine);
}

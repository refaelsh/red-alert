#pragma once
#include <vector>
#include "Coordinate.h"
#include "IRandomizer.h"


class Randomizer : public IRandomizer
{
public:
    explicit Randomizer(std::vector<Coordinate> forbidden_coordinates);
    ~Randomizer() override = default;

    std::vector<Coordinate> Randomize(unsigned int vessel_size) override;

private:
    std::vector<Coordinate> m_forbidden_coordinates;

    bool Is_at_least_one_forbidden(const std::vector<Coordinate>& coordinates);
    bool Is_horizontal_was_randomized();
    unsigned int Pick_a_random_number(int start, size_t end);
};

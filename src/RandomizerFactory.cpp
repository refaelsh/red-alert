#include "RandomizerFactory.h"
#include "randomizer.h"


std::shared_ptr<IRandomizer> RandomizerFactory::Create(std::vector<Coordinate> forbidden_coordinates)
{
    return std::make_shared<Randomizer>(forbidden_coordinates);
}

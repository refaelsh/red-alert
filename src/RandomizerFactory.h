#pragma once
#include "IRandomizerFactory.h"


class RandomizerFactory : public IRandomizerFactory
{
public:
    ~RandomizerFactory() override = default;

    std::shared_ptr<IRandomizer> Create(std::vector<Coordinate> forbidden_coordinates) override;
};

#include "Vessel.h"
#include <utility>


Vessel::Vessel(std::vector<Coordinate> coordinates): m_coordinates(std::move(coordinates))
{
    m_disallowed_coordinates = this->Calculate_forbidden_coordinates_around_the_vessel();
}

std::vector<Coordinate> Vessel::Calculate_forbidden_coordinates_around_the_vessel()
{
    auto res = std::vector<Coordinate>();

    if (Is_horizontal() == true)
    {
        res.emplace_back(Coordinate(m_coordinates[0].Get_x() - 1, m_coordinates[0].Get_y()));
        res.emplace_back(Coordinate(m_coordinates[0].Get_x() + (int)m_coordinates.size(), m_coordinates[0].Get_y()));

        for (int i = 0; i < m_coordinates.size(); ++i)
        {
            res.emplace_back(Coordinate(m_coordinates[0].Get_x() + i, m_coordinates[0].Get_y() + 1));
            res.emplace_back(Coordinate(m_coordinates[0].Get_x() + i, m_coordinates[0].Get_y() - 1));
        }
    }
    else
    {
        res.emplace_back(Coordinate(m_coordinates[0].Get_x(), m_coordinates[0].Get_y() - 1));
        res.emplace_back(Coordinate(m_coordinates[0].Get_x(), m_coordinates[0].Get_y() + (int)m_coordinates.size()));

        for (int i = 0; i < m_coordinates.size(); ++i)
        {
            res.emplace_back(Coordinate(m_coordinates[0].Get_x() + 1, m_coordinates[0].Get_y() + i));
            res.emplace_back(Coordinate(m_coordinates[0].Get_x() - 1, m_coordinates[0].Get_y() + i));
        }
    }

    res = Discard_out_of_board_coordinates(res);

    return res;
}

std::vector<Coordinate> Vessel::Get_coordinates() const
{
    return m_coordinates;
}

std::vector<Coordinate> Vessel::Get_forbidden_coordinates_around_the_vessel() const
{
    return m_disallowed_coordinates;
}

std::vector<Coordinate> Vessel::Get_forbidden_coordinates_including_the_vessel() const
{
    auto res = this->Get_forbidden_coordinates_around_the_vessel();

    for (unsigned int i = 0; i < m_coordinates.size(); ++i)
    {
        res.emplace_back(m_coordinates[i]);
    }

    return res;
}

bool Vessel::Is_horizontal()
{
    if (m_coordinates.size() == 1)
    {
        return true;
    }

    if (m_coordinates[0].Get_y() == m_coordinates[1].Get_y())
    {
        return true;
    }

    return false;
}

std::vector<Coordinate> Vessel::Discard_out_of_board_coordinates(const std::vector<Coordinate>& coordinates)
{
    auto res = std::vector<Coordinate>();

    for (auto coordinate : coordinates)
    {
        if (coordinate.Get_x() >= 0 && coordinate.Get_x() <= 10 &&
            coordinate.Get_y() >= 0 && coordinate.Get_y() <= 10)
        {
            res.emplace_back(coordinate);
        }
    }

    return res;
}

#pragma once
#include <vector>
#include "Coordinate.h"


class Vessel
{
public:
    explicit Vessel(std::vector<Coordinate> coordinates);

    std::vector<Coordinate> Get_coordinates() const;

    std::vector<Coordinate> Get_forbidden_coordinates_around_the_vessel() const;
    std::vector<Coordinate> Get_forbidden_coordinates_including_the_vessel() const;

private:
    std::vector<Coordinate> m_coordinates;
    std::vector<Coordinate> m_disallowed_coordinates;

    std::vector<Coordinate> Calculate_forbidden_coordinates_around_the_vessel();
    bool Is_horizontal();
    std::vector<Coordinate> Discard_out_of_board_coordinates(const std::vector<Coordinate>& coordinates);
};

﻿#include <iostream>
#include "main_view_model.h"
#include "RandomizerFactory.h"


int main()
{
    std::cout << "Press enter to generate a new board.\n";

    const auto randomizer_factory = std::make_shared<RandomizerFactory>();
    const auto game = std::make_shared<Game>(randomizer_factory.get());
    auto main_view_model = std::make_shared<MainViewModel>(game.get());
  
    while (std::cin.get() == '\n')
    {
        std::cout << main_view_model->Randomize_a_new_board();
        std::cout << "------------------------------"<< std::endl;

        std::cout << std::endl;
        std::cout << "--------------------------------------------------------------------------------"<< std::endl;
        std::cout << std::endl;
    }
}

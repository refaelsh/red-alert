#include "main_view_model.h"
#include <utility>


MainViewModel::MainViewModel(Game* game) : m_game(std::move(game))
{
}

std::string MainViewModel::Randomize_a_new_board()
{
    auto all_coordinates = std::vector<Coordinate>();

    m_game->Place_vessels();

    std::vector<Vessel> vessels = m_game->Get_all_vessels();

    for (auto vessel : vessels)
    {
        for (int i = 0; i < vessel.Get_coordinates().size(); ++i)
        {
            all_coordinates.emplace_back(vessel.Get_coordinates()[i]);
        }
    }

    std::string res;

    for (int x = 0; x < 10; ++x)
    {
        res = res + "-------------------------------\n";
        for (int y = 0; y < 10; ++y)
        {
            if (Is_coordinate_a_vessel(Coordinate(x, y), all_coordinates) == true)
            {
                res = res + "|* ";
            }
            else
            {
                res = res + "|  ";
            }

            if (y == 9)
            {
                res = res + "|";
            }
        }

        res = res + "\n";
    }

    return res;
}

bool MainViewModel::Is_coordinate_a_vessel(const Coordinate& coordinate, std::vector<Coordinate> all_coordinates)
{
    for (int i = 0; i < all_coordinates.size(); ++i)
    {
        if (coordinate == all_coordinates[i])
        {
            return true;
        }
    }

    return false;
}

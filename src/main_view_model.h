#pragma once
#include <memory>
#include <string>
#include "game.h"


class MainViewModel
{
public:
    explicit MainViewModel(Game* game);

    std::string Randomize_a_new_board();

private:
    Game* m_game;

    bool Is_coordinate_a_vessel(const Coordinate& coordinate, std::vector<Coordinate> all_coordinates);
};

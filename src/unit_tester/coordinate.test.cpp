#include "game.h"
#include "main_view_model.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"


using namespace testing;

class CoordinateTest : public Test
{
protected:
    CoordinateTest()
    {
        m_cut = std::make_shared<Coordinate>(7, 7);
    }

    ~CoordinateTest() = default;

    std::shared_ptr<Coordinate> m_cut; // cut - it stands for Class Under Test.
};

TEST_F(CoordinateTest, Check_for_equality) // NOLINT
{
    const auto coor1 = Coordinate(7, 7);
    const auto coor2 = Coordinate(7, 7);

    ASSERT_TRUE(coor1 == coor2);
}

TEST_F(CoordinateTest, Check_for_inequality) // NOLINT
{
    const auto coor1 = Coordinate(7, 7);
    const auto coor2 = Coordinate(7, 8);

    ASSERT_TRUE(!(coor1 == coor2));
}

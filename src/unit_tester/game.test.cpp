#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Game.h"
#include "RandomizerFactory.h"
#include "view_model_mocks.h"


using namespace testing;

class GameTest : public Test
{
protected:
    GameTest()
    {
        // m_game = std::make_shared<Game>(m_mock_randomizer_factory.get());
        m_game = std::make_shared<Game>(m_randomizer_factory.get());
    }

    ~GameTest() = default;

    bool Is_horizontal(std::vector<Coordinate> coordinates)
    {
        if (coordinates.size() == 1)
        {
            return true;
        }

        if (coordinates[0].Get_y() == coordinates[1].Get_y())
        {
            return true;
        }

        return false;
    }

    // std::shared_ptr<NiceMock<MockRandomizerFactory>> m_mock_randomizer_factory = std::make_shared<NiceMock<
    //     MockRandomizerFactory>>();
    std::shared_ptr<IRandomizerFactory> m_randomizer_factory = std::make_shared<RandomizerFactory>();
    std::shared_ptr<Game> m_game;
};

TEST_F(GameTest, Clear_all_vessels) // NOLINT
{
    m_game->Clear_all_vessels();

    const auto res = m_game->Get_all_vessels();

    ASSERT_TRUE(res.empty());
}

TEST_F(GameTest, Check_ship_placement) // NOLINT
{
    m_game->Clear_all_vessels();

    m_game->Place_vessels();

    const auto all_vessels = m_game->Get_all_vessels();

    auto all_forbidden_coordinates = std::vector<Coordinate>();
    for (auto vessel : all_vessels)
    {
        auto forbidden_coordinates = vessel.Get_forbidden_coordinates_around_the_vessel();
        for (auto forbidden_coordinate : forbidden_coordinates)
        {
            all_forbidden_coordinates.emplace_back(forbidden_coordinate);
        }
    }

    for (auto vessel : all_vessels)
    {
        auto coordinates = vessel.Get_coordinates();

        for (auto forbidden_coordinate : all_forbidden_coordinates)
        {
            for (auto coordinate : coordinates)
            {
                if (coordinate == forbidden_coordinate)
                {
                    FAIL();
                }
            }
        }
    }
}

TEST_F(GameTest, Check_vessel_sizes) // NOLINT
{
    m_game->Place_vessels();

    const auto res = m_game->Get_all_vessels();

    ASSERT_TRUE(res[0].Get_coordinates().size() == 3);
    ASSERT_TRUE(res[1].Get_coordinates().size() == 2);
    ASSERT_TRUE(res[2].Get_coordinates().size() == 1);
}

TEST_F(GameTest, Check_vessels_amount) // NOLINT
{
    m_game->Place_vessels();

    const auto res = m_game->Get_all_vessels();

    ASSERT_TRUE(res.size() == 3);
}

TEST_F(GameTest, Check_vessels_location_inside_board) // NOLINT
{
    m_game->Place_vessels();

    const auto res = m_game->Get_all_vessels();

    for (int i = 0; i < res.size(); ++i)
    {
        auto coordinates = res[i].Get_coordinates();
        for (auto coordinate : coordinates)
        {
            ASSERT_TRUE(coordinate.Get_x() >= 0);
            ASSERT_TRUE(coordinate.Get_x() <= 10);
            ASSERT_TRUE(coordinate.Get_y() >= 0);
            ASSERT_TRUE(coordinate.Get_y() <= 10);
        }
    }
}

TEST_F(GameTest, Check_vessel_straightness) // NOLINT
{
    m_game->Place_vessels();

    const auto res = m_game->Get_all_vessels();

    for (int i = 0; i < res.size(); ++i)
    {
        auto coordinates = res[i].Get_coordinates();
        if (coordinates.size() > 1)
        {
            for (int j = 0; j < coordinates.size() - 1; ++j)
            {
                if (Is_horizontal(coordinates) == true)
                {
                    ASSERT_TRUE(coordinates[j].Get_y() == coordinates[j + 1].Get_y());
                }
                else
                {
                    ASSERT_TRUE(coordinates[j].Get_x() == coordinates[j + 1].Get_x());
                }
            }
        }
    }
}

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "game.h"
#include "main_view_model.h"
#include "RandomizerFactory.h"
#include "view_model_mocks.h"


using namespace testing;

class MainViewModelTest : public Test
{
protected:
    MainViewModelTest()
    {
        // const auto game = std::make_shared<Game>(m_mock_randomizer_factory.get());
        const auto game = std::make_shared<Game>(m_randomizer_factory.get());
        m_cut = std::make_shared<MainViewModel>(game.get());
    }

    ~MainViewModelTest() = default;

    // std::shared_ptr<NiceMock<MockRandomizerFactory>> m_mock_randomizer_factory = std::make_shared<NiceMock<
    //     MockRandomizerFactory>>();
    std::shared_ptr<IRandomizerFactory> m_randomizer_factory = std::make_shared<RandomizerFactory>();
    std::shared_ptr<MainViewModel> m_cut; // cut - it stands for Class Under Test.
};

TEST_F(MainViewModelTest, Randomize_a_new_board) // NOLINT
{
    //TODO: Finish this test if time remains.
}

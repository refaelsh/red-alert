#pragma once
#include "IRandomizerFactory.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"


class MockRandomizerFactory : public IRandomizerFactory
{
public:
    MockRandomizerFactory() = default;
    ~MockRandomizerFactory() = default;

    MOCK_METHOD1(Create, std::shared_ptr<IRandomizer>(std::vector<Coordinate> forbidden_coordinates));
};
